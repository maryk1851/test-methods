package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.UUID;

public class PlanManager extends ParentManager {
    public PlanManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 не содержит спец символов. Имена не пересекаются
     * друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public PlanPojo createPlan(PlanPojo plan) {
        Validate.isTrue(plan.name.length() > 1 && plan.name.length() < 129, "Name's length should be " +
                "more or equal 2 and less or equal 128");
        return dbService.createPlan(plan);
    }

    public PlanPojo updatePlan(PlanPojo plan) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removePlan(UUID id) {
        throw new NotImplementedException("Please implement the method.");
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<PlanPojo> getPlans(UUID customerId) {
        throw new NotImplementedException("Please implement the method.");
    }
}
