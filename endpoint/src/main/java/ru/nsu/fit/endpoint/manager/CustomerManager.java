package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы
     * строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы
     * строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же e-mail;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать
     * части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");

        Validate.notNull(customer.pass);
        Validate.isTrue(customer.pass.length() >= 6 && customer.pass.length() < 13, "Password's " +
                "length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.pass.equalsIgnoreCase("123qwe")
                && !customer.pass.equalsIgnoreCase("1q2w3e"), "Password is easy.");
        Validate.isTrue(!customer.pass.toLowerCase().contains(customer.login.toLowerCase())
                && !customer.pass.toLowerCase().contains(customer.firstName.toLowerCase())
        && !customer.pass.toLowerCase().contains(customer.lastName.toLowerCase()),
                "Password contains customer's login or last name of first name");

        validateFirstNameLastName(customer);

        Validate.isTrue(customer.login.matches("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+" +
                "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"), "Incorrect e-mail");
        Validate.isTrue(!dbService.isExistCustomerWithLogin(customer.login), "This login already exist");

        Validate.isTrue(customer.balance == 0, "Balance not equal 0");

        // TODO: необходимо дописать дополнительные проверки

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<CustomerPojo> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public CustomerPojo updateCustomer(CustomerPojo customer) {
        Validate.notNull(customer.id, "Costumer ID is null");

        validateFirstNameLastName(customer);
        Validate.isTrue(dbService.updateCustomer(customer) > 0, "There is no costumer with id");

        return customer;
    }

    public void removeCustomer(UUID id) {
        Validate.notNull(id, "Costumer ID is null");

        Validate.isTrue(dbService.deleteCustomer(id) > 0, "There is no costumer with id");
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public CustomerPojo topUpBalance(UUID customerId, int amount) {
        Validate.notNull(customerId, "Costumer ID is null");

        Validate.isTrue(amount > 0, "Amount should be more then 0");

        Validate.isTrue(dbService.topUpCustomerBalance(customerId, amount) > 0, "There is no costumer with id");

        return dbService.getCustomerById(customerId);
    }

    private void validateFirstNameLastName(CustomerPojo customer) {
        Validate.isTrue(customer.firstName.length() > 2, "First name is short.");
        Validate.isTrue(customer.firstName.length() < 13, "First name is long.");
        Validate.isTrue(!customer.firstName.contains(" "), "First name contains spaces.");
        Validate.isTrue(customer.firstName.matches("[A-Z][a-z]+"), "First name contains incorrect " +
                "symbols");

        Validate.isTrue(customer.lastName.length() > 2, "Last name is short.");
        Validate.isTrue(customer.lastName.length() < 13, "Last name is long.");
        Validate.isTrue(!customer.lastName.contains(" "), "Last name contains spaces.");
        Validate.isTrue(customer.lastName.matches("[A-Z][a-z]+"), "Last name contains incorrect " +
                "symbols");
    }
}
