package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class CustomerManagerTest {
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);
//        dbService = new DBService(logger);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateCustomer() {
        // настраиваем mock.
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@gmail.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    // Как не надо писать тест...
    // Используйте expected exception аннотации или expected exception rule...
    @Test
    public void testCreateCustomerWithNullArgument_Wrong() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNullArgument_Right() {
        customerManager.createCustomer(null);
    }

    @Test
    public void testCreateCustomerWithEasyPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwe";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Password is easy.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerWithShortLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "N";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name is short.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerWithLongLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Natalyismails";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name is long.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerLastNameWithSpace() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wi ck";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name contains spaces.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerLastNameWithIncorrectSymbolsCounts() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick1";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerLastNameWithIncorrectSymbolsFirstLowerCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerLastNameWithIncorrectSymbolsNotFirstUpperCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "WicK";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerLastNameWithIncorrectSymbols() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick:";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerWithShortFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "N";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name is short.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerWithLongFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Natalyismails";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name is long.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerFirstNameWithSpace() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Jo hn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name contains spaces.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerFirstNameWithIncorrectSymbolsCounts() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Jo1hn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerFirstNameWithIncorrectSymbolsFirstLowerCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "john";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerFirstNameWithIncorrectSymbolsNotFirstUpperCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "JOhn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerFirstNameWithIncorrectSymbols() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Jo_hn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name contains incorrect symbols");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerWithIncorrectEmail() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@.gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Incorrect e-mail");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerWithDuplicateEmail() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        when(dbService.isExistCustomerWithLogin(createCustomerInput.login)).thenReturn(true);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("This login already exist");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerShortPass() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "15gbi";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 " +
                "symbols.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerLongPass() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "15gbih59s2k64";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Password's " +
                "length should be more or equal 6 symbols and less or equal 12 symbols.");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testCreateCustomerPassContainsLoginLastnameFirstname() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "joHn_wiCk";
        createCustomerInput.balance = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Password contains customer's login or last name of first name");

        customerManager.createCustomer(createCustomerInput);
    }

    @Test
    public void testUpdateCustomerNullID() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.id = null;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Costumer ID is null");

        customerManager.updateCustomer(createCustomerInput);
    }

    @Test
    public void testUpdateCustomerNoSuchID() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";

        when(dbService.updateCustomer(createCustomerInput)).thenReturn(0);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("There is no costumer with id");

        customerManager.updateCustomer(createCustomerInput);
    }

    @Test
    public void testRemoveCustomerNullID() {
        UUID id = null;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Costumer ID is null");

        customerManager.removeCustomer(id);
    }

    @Test
    public void testRemoveCustomerNoSuchID() {
        UUID id = UUID.randomUUID();

        when(dbService.updateCustomer(createCustomerInput)).thenReturn(0);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("There is no costumer with id");

        customerManager.removeCustomer(id);
    }

    @Test
    public void testTopUpBalanceNullId() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = null;
        createCustomerInput.balance = 5;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Costumer ID is null");

        customerManager.topUpBalance(createCustomerInput.id, createCustomerInput.balance);
    }

    @Test
    public void testTopUpBalanceZeroAmount() {
        UUID id = UUID.randomUUID();
        int amount = 0;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Amount should be more then 0");

        customerManager.topUpBalance(id, amount);
    }

    @Test
    public void testTopUpBalanceNoSuchID() {
        UUID id = UUID.randomUUID();
        int amount = 5;

        when(dbService.topUpCustomerBalance(id, amount)).thenReturn(0);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("There is no costumer with id");

        customerManager.topUpBalance(id, amount);
    }
}
